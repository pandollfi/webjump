<?php
@ob_start();
@session_start();
require_once('header.php');
require_once('../src/dao/categoria_dao.php');
$categoriaDao = new CategoriaDao();

?>
<script src="<?php echo URL; ?> . 'js/cadastro_produtos.js"></script>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card card-primary">
                    <div class="alert" role="alert" style="display:none;" id="modalMsg"></div>
                    <div class="card-header">
                        <h3 class="card-title">Cadastro de Produto</h3>
                    </div>
                    <form method="POST" role="form" data-form>
                        <input type="hidden" id="referencia" name="referencia" value="produtos">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="titulo">Nome</label>
                                        <input type="text" class="form-control" id="nome" name="nome" placeholder="Digite o nome do produto" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="telefone">Descrição</label>
                                        <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Descreva o produto" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="endereco">SKU</label>
                                        <input type="text" class="form-control" id="sku" name="sku" placeholder="Digite o código do produto (SKU)" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="cep">Preço</label>
                                        <input type="text" class="form-control" id="preco" name="preco" onKeyPress="return(mvalor(this,'.',',',event))" placeholder=" Digite o preço do produto" maxlength="15" required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cidade">Quantidade</label>
                                        <input type="text" class="form-control" id="quantidade" name="quantidade" placeholder="Digite a quantidade" required>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="categoria">Categoria(s):</label>
                                        <select class="select form-control" id="categoria[]" name="categoria[]" multiple="multiple">
                                            <?php
                                            echo $categoriaDao->carrega_categorias_lista();
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer">
                                <div class="d-flex justify-content-center">
                                    <input type="submit" class="btn btn-primary " id="btn-acao" value="Cadastrar" data-btn-acao>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function setup() {
        document.querySelector('[data-btn-acao]').addEventListener('click', enviaFormulario);
    }

    setup();
</script>
<?php
require 'footer.php';
?>