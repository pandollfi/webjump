<?php

require_once('../dao/produto_dao.php');
require_once('../dao/categoria_dao.php');
require_once('../dao/busca_dao.php');
require_once('../dao/login_dao.php');
require_once('../model/classe_produto.php');
require_once('../model/classe_categoria.php');
require_once('../model/classe_busca.php');
require_once('../model/classe_login.php');
require_once('../helper/funcoes.php');
require_once('../../autoload.php');

Autoload::carrega_const();

$func = new Funcoes();

$retorno = array();

$dados = $_POST;

if ($dados['referencia'] == 'produtos') {
    $valida_campos = $func->valida_campo($dados);
    if ($valida_campos['cod'] == 222) {
        $valor = $func->trata_preco($dados['preco']);
        $produto = new Produto($dados['nome'], $dados['sku'], $valor, $dados['descricao'], $dados['quantidade'], $dados['categoria']);
        $produto_dao = new ProdutoDao();
        $categoria_dao = new CategoriaDao();
        if ($id_produto = $produto_dao->inserir($produto)) {
            foreach ($dados['categoria'] as $categoria) {
                $categoria_dao->atribuirCategoria($categoria, $id_produto);
            }
            $retorno['cod'] = 111;
        } else {
            $retorno['cod'] = 999;
        }
    } else {
        $retorno['cod'] = $valida_campos['cod'];
    }
    $trata_retorno = $func->retorno_codigo($retorno['cod']);
    echo json_encode(array("cod" => $trata_retorno['cod'], "descricao" => $trata_retorno['desc'], "estilo" => $trata_retorno['estilo']));
} elseif ($dados['referencia'] == 'categorias') {
    $valida_campos = $func->valida_campo($dados);
    if ($valida_campos['cod'] == 222) {
        $categoria = new Categoria($dados['descricao']);
        $categoria_dao = new CategoriaDao();
        if ($categoria_dao->inserir($categoria)) {
            $retorno['cod'] = 111;
        } else {
            $retorno['cod'] = 999;
        }
    } else {
        $retorno['cod'] = $valida_campos['cod'];
    }
    $trata_retorno = $func->retorno_codigo($retorno['cod']);
    echo json_encode(array("cod" => $trata_retorno['cod'], "descricao" => $trata_retorno['desc'], "estilo" => $trata_retorno['estilo']));
} elseif ($dados['referencia'] == 'busca') {
    $palavra = $dados['busca'];
    $busca = new Busca($palavra);
    $busca_dao = new BuscaDao();
    $retorno = $busca_dao->busca($busca);
    if (empty($retorno)) {
        $retorno['cod'] = 998;
        $trata_retorno = $func->retorno_codigo($retorno['cod']);
        echo json_encode($trata_retorno);
        exit;
    }
    if (empty($palavra)) {
        $retorno['cod'] = 997;
        $trata_retorno = $func->retorno_codigo($retorno['cod']);
        echo json_encode($trata_retorno);
        exit;
    }
    $trata_busca = $busca_dao->trata_retorno_busca($retorno);
    echo json_encode($trata_busca);
} elseif ($dados['referencia'] == 'login') {
    $valida_campos = $func->valida_campo($dados);
    if ($valida_campos['cod'] == 222) {
        $login = new Login($dados['login'], $dados['senha']);
        $login_dao = new LoginDao();
        $retorno = $login_dao->acesso($login);
        $trata_retorno = $func->retorno_codigo($retorno['cod']);
        echo json_encode(array("cod" => $trata_retorno['cod'], "descricao" => $trata_retorno['desc'], "estilo" => $trata_retorno['estilo']));
    } else {
        $trata_retorno = $func->retorno_codigo($valida_campos['cod']);
        echo json_encode(array("cod" => $trata_retorno['cod'], "descricao" => $trata_retorno['desc'], "estilo" => $trata_retorno['estilo']));
    }
}
