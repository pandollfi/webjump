<?php

class Categoria
{

    private $descricao;

    public function __construct($descricao)
    {
        $this->descricao = $descricao;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }


    public function setDescricao($descricao)
    {
        $this->nome = $descricao;

        return $this;
    }

    public function carrega_categorias()
    {
        require_once('../dao/produto_dao.php');
        require_once('../db/classe_db.php');
        $this->db = new Database();
        $this->db->connect();
    }

    public function getArrayCategoriaProduto()
    {
        $data = [
            "id_produtos" => $this->id_produto,
            "id_categorias" => $this->id_categoria,
            "datacadastro" => date('Y-m-d H:i:s')
        ];
        return $data;
    }

    public function getArrayCategoria()
    {
        $data = [
            "descricao" => $this->descricao,
            "datacadastro" => date('Y-m-d H:i:s')
        ];
        return $data;
    }
}
