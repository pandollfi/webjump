<?php

class Produto
{

    private $id;
    private $nome;
    private $sku;
    private $preco;
    private $descricao;
    private $quantidade;
    private $categorias;

    public function __construct($nome, $sku, $preco, $descricao, $quantidade, $categorias)
    {
        $this->id = 0;
        $this->nome = $nome;
        $this->sku = $sku;
        $this->preco = $preco;
        $this->descricao = $descricao;
        $this->quantidade = $quantidade;
        $this->categorias = $categorias;
    }



    public function getArray()
    {
        $data = [
            "nome" => $this->nome,
            "sku" => $this->sku,
            "preco" => $this->preco,
            "descricao" => $this->descricao,
            "quantidade" => $this->quantidade,
            "descricao" => $this->descricao,
            "datacadastro" => date('Y-m-d H:i:s')
        ];
        return $data;
    }





    public function getNome()
    {
        return $this->nome;
    }


    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }


    public function getId()
    {
        return $this->id;
    }


    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }


    public function getSku()
    {
        return $this->sku;
    }

    public function getPreco()
    {
        return $this->preco;
    }


    public function setPreco($preco)
    {
        $this->preco = $preco;

        return $this;
    }


    public function getCategorias()
    {
        return $this->categorias;
    }


    public function setCategorias($categorias)
    {
        $this->categorias = $categorias;

        return $this;
    }


    public function getQuantidade()
    {
        return $this->quantidade;
    }


    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;

        return $this;
    }
}
