<?php


class ProdutoDao
{

    private $db;

    public function __construct()
    {
        require_once(RAIZ . 'src/db/classe_db.php');
        $this->db = new Database();
    }

    public function inserir(Produto $produto)
    {
        $this->db->connect();
        return $this->db->insert('produtos_tbl', $produto->getArray());
    }

    public function carrega_produtos($condicao = null)
    {
        $this->db->connect();
        $query = 'SELECT * FROM produtos_tbl ORDER BY id ';
        $resultado = $this->db->fetch_all($query);
        return $resultado;
    }

    public function lista_produtos_tabela()
    {
        $option = null;
        $resultado = $this->carrega_produtos();
        if (empty($resultado)) {
            $option .= "<div class='card-body'>
                            <div class='d-flex justify-content-center'>Nenhum resultado encontrado...</div>
                        </div>";
        } else {
            $option .= "<div class='card-body'>
                    <table class='table table-hover table-bordered results'>
                        <thead>
                            <tr>
                                <th class='col-md-1'>ID</th>
                                <th class='col-md-4'>Nome</th>
                                <th class='col-md-2'>SKU</th>
                                <th class='col-md-3'>Preço</th>
                                <th class='col-md-3'>Quantidade</th>
                            </tr>
                        </thead>
                  ";
            $option .= "<tbody>";
            foreach ($resultado as $result) {
                $option .= "<tr>
                            <th class='col-md-1'>" . $result['id'] . "</th>
                            <td class='col-md-5'>" . $result['nome'] . "</td>
                            <td class='col-md-2'>" . $result['sku'] . "</td>
                            <td class='col-md-3'>R$ " . $result['preco'] . "</td>
                            <td class='col-md-3'>" . $result['quantidade'] . "</td>
                        </tr>";
            }
            $option .= "</tbody>
                     </table>
                </div>";
        }

        return $option;
    }
}
