<?php


class CategoriaDao
{

    private $id_produto;
    private $id_categoria;

    public function __construct()
    {
        require_once(RAIZ . 'src/db/classe_db.php');
        $this->db = new Database();
    }


    public function carrega_categorias_lista($condicao = null)
    {
        $option = null;
        $this->db->connect();
        $query = 'SELECT * FROM categorias_tbl';
        $resultado = $this->db->fetch_all($query);
        if (!empty($resultado)) {
            foreach ($resultado as $result) {
                $option .= "<option
             value=" . $result['id'] . ">" . $result['descricao'] . "</option>";
            }
        } else {
            $option = "<option value='' disabled> Não há categorias cadastradas! </option>";
        }
        return $option;
    }


    public function inserir(Categoria $categoria)
    {
        $this->db->connect();
        return $this->db->insert('categorias_tbl', $categoria->getArrayCategoria());
    }

    public function atribuirCategoria($categoria, $id_produto)
    {
        $this->db->connect();
        $dados = [
            "id_categorias" => intval($categoria),
            "id_produtos" => intval($id_produto)
        ];
        return $this->db->insert('produto_categorias_tbl', $dados);
    }



    public function getId_produto()
    {
        return $this->id_produto;
    }

    public function setId_produto($id_produto)
    {
        $this->id_produto = $id_produto;

        return $this;
    }


    public function getId_categoria()
    {
        return $this->id_categoria;
    }


    public function setId_categoria($id_categoria)
    {
        $this->id_categoria = $id_categoria;

        return $this;
    }


    public function inserir_categoria_produto(Categoria $categoria)
    {
        $this->db->connect();
        return $this->db->insert('produtos_tbl', $categoria->getArrayCategoriaProduto());
    }

    public function carrega_categorias($condicao = null)
    {
        $this->db->connect();
        $query = 'SELECT * FROM categorias_tbl ORDER BY id ';
        $resultado = $this->db->fetch_all($query);
        return $resultado;
    }

    public function lista_categorias_tabela()
    {
        $option = null;
        $resultado = $this->carrega_categorias();
        if (empty($resultado)) {
            $option .= "<div class='card-body'>
                            <div class='d-flex justify-content-center'>Nenhum resultado encontrado...</div>
                        </div>";
        } else {
            $option .= "<div class='card-body'>
                            <table class='table table-hover table-bordered results'>
                                <thead>
                                    <tr>
                                    <th class='col-md-3'>ID</th>
                                    <th class='col'>Categoria</th>
                                    </tr>
                                </thead>
                        ";
            $option .= "<tbody>";
            foreach ($resultado as $result) {
                $option .= "<tr>
                            <th class='col-md-1'>" . $result['id'] . "</th>
                            <td class='col-md-5'>" . $result['descricao'] . "</td>
                        </tr>";
            }
            $option .= "</tbody>
                    </table>
                </div>";
        }
        return $option;
    }
}
