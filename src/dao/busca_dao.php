<?php


class BuscaDao
{

    private $db;

    public function __construct()
    {
        require_once(RAIZ . 'src/db/classe_db.php');
        $this->db = new Database();
    }

    public function busca(Busca $busca)
    {
        $array_palavra = $busca->getArrayBusca();
        $palavra = $array_palavra['busca'];

        $this->db->connect();
        $query = "SELECT pro.id, pro.nome, pro.descricao, pro.quantidade, pro.sku, pro.preco,
                    cat.descricao as categoria FROM produtos_tbl pro
                    left join produto_categorias_tbl procat on pro.id = procat.id_produtos
                    left join categorias_tbl cat on cat.id = procat.id_categorias
                    where pro.nome like '%" . $palavra . "%' or pro.descricao like '%" . $palavra . "%' 
                    or pro.sku  like '%" . $palavra . "%' or cat.descricao like '%" . $palavra . "%' 
                    group by pro.id order by pro.id asc";
        $busca = $this->db->fetch_all($query);
        return $busca;
    }

    public function trata_retorno_busca($resultados)
    {
        $contador = 0;
        $produto = null;
        if (!empty($resultados)) {
            $produto[] = '<div class="row">';
            foreach ($resultados as $resultado) {
                $id_encode = base64_encode($resultado['id']);
                $produto[] = '<div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0">
                            <div class="icon-box">
                                <div class="icon"><i class="bx bx-arch"></i></div>
                                <h4><a href="produto_detalhe.php?i=' . $id_encode . '">' . $resultado['nome'] . '</a></h4>
                                <p>Clique para ver mais sobre a produto</p>
                            </div>
                        </div>';
            }
            $produto[] = '</div>';
            return $produto;
        } else {
        }
    }

    public function lista_produtos_tabela() /////////
    {
        $option = null;
        $resultado = $this->carrega_produtos();
        if (empty($resultado)) {
            $option .= "<div class='card-body'>
                            <div class='d-flex justify-content-center'>Nenhum resultado encontrado...</div>
                        </div>";
        } else {
            $option .= "<div class='card-body'>
                    <table class='table table-hover table-bordered results'>
                        <thead>
                            <tr>
                                <th class='col-md-1'>ID</th>
                                <th class='col-md-5'>Título</th>
                                <th class='col-md-2'>Telefone</th>
                                <th class='col-md-3'>Cidade</th>
                                <th class='col-md-1'>Ações</th>
                            </tr>
                        </thead>
                  ";
            $option .= "<tbody>";
            foreach ($resultado as $result) {
                $option .= "<tr>
                            <th class='col-md-1'>" . $result['id'] . "</th>
                            <td class='col-md-5'>" . $result['razaosocial'] . "</td>
                            <td class='col-md-2'>" . $result['telefone'] . "</td>
                            <td class='col-md-3'>" . $result['cidade'] . "</td>
                            <td class='col-md-1'>Editar</td>
                        </tr>";
            }
            $option .= "</tbody>
                     </table>
                </div>";
        }

        return $option;
    }

    public function carrega_produto_detalhe($id)
    {
        $this->db->connect();
        $query = "SELECT pro.id, pro.nome, pro.descricao, pro.preco, pro.quantidade, pro.sku
                     FROM produtos_tbl pro where pro.id = " . $id;
        $busca = $this->db->fetch_all($query);
        $produto = $busca[0];
        $trata_categorias = $this->carrega_categoria_produto_detalhe($produto['id']);
        $produto['categorias'] = $trata_categorias;

        return $produto;
    }

    public function carrega_categoria_produto_detalhe($id)
    {
        $categorias = null;
        $this->db->connect();
        $query = "SELECT 
                cat.descricao
            FROM
                categorias_tbl cat
                    LEFT JOIN
                produto_categorias_tbl procat ON cat.id = procat.id_categorias
                    LEFT JOIN 	
                produtos_tbl pro on pro.id = procat.id_produtos
                where pro.id = " . $id;
        $busca = $this->db->fetch_all($query);
        foreach ($busca as $categoria) {
            $categorias .= " " . $categoria['descricao'] . ",";
        }
        $categorias = substr($categorias, 0, -1);

        return $categorias;
    }
}
