SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Banco de dados: `webjump`
--
CREATE DATABASE IF NOT EXISTS `webjump` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `webjump`;

-- --------------------------------------------------------

--
-- Estrutura para tabela `categorias_tbl`
--

CREATE TABLE `categorias_tbl` (
  `id` int(11) NOT NULL,
  `descricao` varchar(200) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT 1,
  `datacadastro` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

- --------------------------------------------------------

--
-- Estrutura para tabela `produtos_tbl`
--

CREATE TABLE `produtos_tbl` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `preco` double(14,2) DEFAULT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `quantidade` int(5) DEFAULT NULL,
  `datacadastro` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `produto_categorias_tbl`
--

CREATE TABLE `produto_categorias_tbl` (
  `id` int(11) NOT NULL,
  `id_produtos` int(11) NOT NULL,
  `id_categorias` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuarios_tbl`
--

CREATE TABLE `usuarios_tbl` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `senha` varchar(100) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `usuarios_tbl`
--

INSERT INTO `usuarios_tbl` (`id`, `nome`, `senha`, `ativo`) VALUES
(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 1);

--
-- Índices para tabelas despejadas
--

--
-- Índices de tabela `categorias_tbl`
--
ALTER TABLE `categorias_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `produtos_tbl`
--
ALTER TABLE `produtos_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `produto_categorias_tbl`
--
ALTER TABLE `produto_categorias_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_produto_categorias_produtos_tbl_idx` (`id_produtos`),
  ADD KEY `fk_produto_categorias_categorias_tbl1_idx` (`id_categorias`);

--
-- Índices de tabela `usuarios_tbl`
--
ALTER TABLE `usuarios_tbl`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT para tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `categorias_tbl`
--
ALTER TABLE `categorias_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de tabela `produtos_tbl`
--
ALTER TABLE `produtos_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de tabela `produto_categorias_tbl`
--
ALTER TABLE `produto_categorias_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de tabela `usuarios_tbl`
--
ALTER TABLE `usuarios_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restrições para tabelas despejadas
--

--
-- Restrições para tabelas `produto_categorias_tbl`
--
ALTER TABLE `produto_categorias_tbl`
  ADD CONSTRAINT `fk_produto_categorias_categorias_tbl1` FOREIGN KEY (`id_categorias`) REFERENCES `categorias_tbl` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_produto_categorias_produtos_tbl` FOREIGN KEY (`id_produtos`) REFERENCES `produtos_tbl` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

